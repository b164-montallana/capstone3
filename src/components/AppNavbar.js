import { Fragment, useContext } from "react";
import { Container, Nav, Navbar } from "react-bootstrap";
import { BsCart3 } from "react-icons/bs";
import { Link } from "react-router-dom";
import UserContext from "../UserContext";


export default function AppNavbar() {

    const {user, setUser} = useContext(UserContext);

    return (
    <Navbar bg="info" expand="md">
    <Container >
        <Navbar.Brand as={Link} to="/">The Camel Trader</Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
            <Nav
                className="col d-flex justify-content-end me-auto my-2 my-lg-0"
                style={{ maxHeight: '100%' }}
                navbarScroll
                >
                <Nav.Link as={Link} to="/" >Home</Nav.Link>
                <Nav.Link as={Link} to="/products" >Products</Nav.Link>
                {(user.id !== null) ?
                <Fragment>
                <Nav.Link as={Link} to="/logout" className="text-danger">Logout</Nav.Link>
                <Nav.Link as={Link} to="/cart" ><BsCart3 className="cartIcon"/></Nav.Link>
                </Fragment>
                :
                <Fragment>
                <Nav.Link as={Link} to="/register" >Sign Up</Nav.Link>
                <Nav.Link as={Link} to="/login" >Login</Nav.Link>
                <Nav.Link as={Link} to="/cart" >
                    <BsCart3 className="cartIcon"/> 
                </Nav.Link>
                </Fragment>
                }
            </Nav>
        </Navbar.Collapse>
    </Container>
  </Navbar>
    )
}




