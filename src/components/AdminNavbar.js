
import { Fragment } from "react";
import { Container, Nav, Navbar } from "react-bootstrap";
import { Link } from "react-router-dom";
import CreateItem from "../pages/CreateItem";

export default function AdminNavbar() {

    // const [sidebar, setSidebar] = useState(false)

    // const showSidebar = () => setSidebar(!sidebar)
  return (
    <Fragment>
        <Navbar bg="info">
            <Navbar.Brand className="mx-5" as={Link} to='/admin'>Admin</Navbar.Brand>
            <Nav.Link as={Link} to='/logout' className="col d-flex justify-content-end px-5 text-danger">Logout</Nav.Link>
        </Navbar>
        
        <Container>
        <Navbar>
            <Nav>
                <Nav.Link as={Link} to='/admin'>Create Product</Nav.Link>
                <Nav.Link as={Link} to='/product2'>Retrieve Product</Nav.Link>
                <Nav.Link>Update Product</Nav.Link>
                <Nav.Link>Delete Product</Nav.Link>
            </Nav>
        </Navbar>
            <CreateItem/>
        </Container>
    </Fragment>
    )
}
