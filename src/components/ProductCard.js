import { Card, Button, Col} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useCart } from 'react-use-cart';


export default function ProductCard({productProp}) {

    const {name, description, price, category, _id} = productProp

    const { addItem } = useCart();
    
    return(
        <Col className='my-4' lg={{span:7, offset:3}}>
        <Card className='d-flex flex-col'>
            <Card.Body className='text-center'>
                <Card.Title>{name}</Card.Title>
                <Card.Text>{description}</Card.Text>
                <Card.Text>Php {price}</Card.Text>
                <Card.Text>{category}</Card.Text>
                <Button variant="info" as={Link} to={`/products/${_id}`}>Details</Button>
            </Card.Body>
        </Card>
        </Col>
    
    );
};











