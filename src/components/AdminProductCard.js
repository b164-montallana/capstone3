
import { Col, Card, Button } from "react-bootstrap";




export default function AdminProductCard({productProps}) {
     const {name, description, price, categories, stock} = productProps

       return (
   
    <Col md={8} className="m-auto p-1">
    <Card >
        <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Text>{description}</Card.Text>
            <Card.Text>{price}</Card.Text>
            <Card.Text>{categories}</Card.Text>
            <Card.Text>{stock}</Card.Text>
        </Card.Body>
    </Card>
    </Col>
   
  )
}

