import { Button, Container, NavDropdown, Form } from "react-bootstrap";
import { BsSearch } from "react-icons/bs";


export default function Searchbar() {
  return (
    <Container className="col-sm-6 mt-4">
    <Form>
        <Form.Group className="d-flex">
            <NavDropdown title={<span className="text-secondary">Categories</span>} id="basic-nav-dropdown">
                <NavDropdown.Item >Necklaces</NavDropdown.Item>
                <NavDropdown.Item >Rings</NavDropdown.Item>
                <NavDropdown.Item >Pendants</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item >All</NavDropdown.Item>
            </NavDropdown>
            <Form.Control
                type="search"
                placeholder="Search Item"
                required
            />
            <Button className="border-secondary bg-info"><BsSearch/></Button>
        </Form.Group>
    </Form>
    </Container>
  )
}
