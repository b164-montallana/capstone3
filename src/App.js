import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { UserProvider } from './UserContext';
import { useState, useEffect } from 'react';
import { CartProvider } from 'react-use-cart';
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import Logout from './pages/Logout';
import Cart from './pages/Cart';
import Products from './pages/Products';
import ErrorPage from './pages/Error';
import Admin from './pages/Admin';
import CreateItem from './pages/CreateItem';
import Product2 from './pages/Product2';
import ProductView from './pages/ProductView';



export default function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(() => {

    fetch("http://localhost:4000/users/details",{
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}` 
      }
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
      })
      }
    })

  }, [] )
  

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
    <CartProvider>
      <Router>
          <Routes>
            <Route path='/admin' element={<Admin/>}/>
            <Route path='/createItem' element={<CreateItem/>}/>
            <Route path='/' element={<Home/>}/>
            <Route path='/cart' element={<Cart/>}/>
            <Route path='/products' element={<Products/>}/>
            <Route path='/product2' element={<Product2/>}/>
            <Route path='/products/:productId' element={<ProductView/>}/>
            <Route path='/login' element={<Login/>}/>
            <Route path='/register' element={<Register/>}/>
            <Route path='/logout' element={<Logout/>}/>
            <Route path='*' element={<ErrorPage/>}/>
          </Routes>
      </Router>
    </CartProvider>
    </UserProvider>
  );
}


