import { Fragment } from "react";
import AppNavbar from "../components/AppNavbar";
import Searchbar from "../components/Searchbar";
import Slider from "../components/Slider";


export default function Home() {
  return (
    <Fragment>
      <AppNavbar/>
      <Searchbar/>
      <Slider/>
    </Fragment>
    

  )
}
