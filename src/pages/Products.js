import ProductCard from '../components/ProductCard';
import { Container } from 'react-bootstrap';
import { Fragment, useState, useEffect } from 'react';
import Searchbar from '../components/Searchbar';
import AppNavbar from '../components/AppNavbar';


export default function Products(){

    const [products, setProducts] = useState([]);

    useEffect(() => {
		fetch("http://localhost:4000/products/")
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setProducts(data.map(product => {
				return(
					<ProductCard key={product._id} productProp={product} />
					)
			}))
		})	
	}, [])

    
    return (
    <Fragment>
        <AppNavbar/>
        <Searchbar/>
        
            <Container>
                {products}
            </Container>
       
    </Fragment>
    );
};
