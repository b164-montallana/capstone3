import { Container, Form, Button, Nav } from "react-bootstrap";
import styled from 'styled-components';
import { Link, useNavigate, Navigate } from "react-router-dom";
import { useState, useEffect, useContext, Fragment } from "react";
import UserContext from "../UserContext";
import Swal from "sweetalert2";
import AppNavbar from "../components/AppNavbar";

const Title = styled.h2`
    font-weight: bold;
    text-align: center;
    padding: 25px;
    margin-top: 3rem;
`

export default function Register() {

    const { user, setUser } = useContext(UserContext);

    const navigate = useNavigate()

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [mobileNo, setMobileNo] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");

    const [isActive, setIsActive] = useState(false);

    function registerUser(e) {

		//prevents page redirection via a form submission
		e.preventDefault();

		fetch("http://localhost:4000/users/checkEmail", {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true) {
				Swal.fire({
					title: "Duplicate email found",
					icon: "error",
					text: "Kindly provide another email to complete the registration"
				})
			} else {

				fetch("http://localhost:4000/users/register", {
					method: "POST",
					headers: {
						"Content-Type": 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {

					if(data === true) {
						//Clear input fields
						setFirstName("");
						setLastName("");
						setEmail("");
						setMobileNo("");
						setPassword1("");
						setPassword2("");

						Swal.fire({
							title: 'Registration Successful',
							icon: 'success',
							text: 'Welcome to Batch 164, Course Booking!'
						})

						navigate("/login")

					} else {

						Swal.fire({
							title: 'Something went wrong!',
							icon: 'error',
							text: 'Please try again.'
						})
					}
				})

			}
		})

	}
    useEffect(()=> {
		//Validation to enable the submit button when all the input fields are populated and both passwords match 
		if((firstName !== "" && lastName !== "" && mobileNo.length === 11 && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName, lastName, mobileNo, email, password1, password2])

    return (
    <Fragment>
    <AppNavbar/>
    {(user.id !== null)?
    <Navigate to ="/products" />
    :
    <Container>
        <Title>Create an account</Title>
        <Container className="col col-10">
            <Form onSubmit= {(e) => registerUser(e)}>
                {/* First Name */}
                <Form.Group>
                    <Form.Label className="pt-2">First Name</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter First Name"
                        value={firstName}
                        onChange={e => setFirstName(e.target.value)}
                        required
                    />
                </Form.Group>
                {/* Last Name */}
                <Form.Group>
                    <Form.Label className="pt-2"s>Last Name</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter Last Name"
                        value={lastName}
                        onChange={e => setLastName(e.target.value)}
                        required
                    />
                </Form.Group>
                {/* Email */}
                <Form.Group>
                    <Form.Label className="pt-2">Email</Form.Label>
                    <Form.Control
                        type="email"
                        placeholder="Enter Email"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                        required
                    />
                </Form.Group>
                {/* Mobile Number */}
                <Form.Group>
                    <Form.Label className="pt-2">Mobile Number</Form.Label>
                    <Form.Control
                        type="number"
                        placeholder="Enter Mobile Number"
                        value={mobileNo}
                        onChange={e => setMobileNo(e.target.value)}
                        required
                    />
                </Form.Group>
                {/* Password-1 */}
                <Form.Group>
                    <Form.Label className="pt-2">Password</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Enter password"
                        value={password1}
                        onChange={e => setPassword1(e.target.value)}
                        required
                    />
                </Form.Group>
                {/* Password-2 */}
                <Form.Group>
                    <Form.Label className="pt-2">Confirm Password</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Confirm password"
                        value={password2}
                        onChange={e => setPassword2(e.target.value)}
                        required
                    />
                </Form.Group>
                { isActive ?
                <Button className="btn btn-block mt-3" variant="info" type="submit">Create account</Button>
                :
                <Button className="btn btn-block mt-3" variant="info" type="submit" disabled>Create account</Button>
                }
                <Nav className="d-flex mt-3">
                    <Nav.Item disabled className="px-2 mb-3">Already have an account?</Nav.Item>
                    <Nav.Item as={Link} to='/login'>Login</Nav.Item>
                </Nav>
            </Form>
        </Container>
    </Container>}
    </Fragment>
  )
}
