import { Container, Form, Button, InputGroup } from "react-bootstrap";
import { Fragment, useContext, useEffect, useState } from "react";
import { BsEnvelope, BsEyeSlash, BsEye } from "react-icons/bs";
import { Navigate } from "react-router-dom";
import Swal from 'sweetalert2';
import UserContext from "../UserContext";
import styled from 'styled-components';
import AppNavbar from "../components/AppNavbar";

const Title = styled.h2`
    font-weight: bold;
    text-align: center;
    padding: 25px;
    margin-top: 3rem;
`


export default function Login() {

    const { user, setUser } = useContext(UserContext);

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isActive, setIsActive] = useState(false);
    const [passwordShown, setPasswordShown] = useState(false);

    function userLogin(e) {

        e.preventDefault();

        /*
        Syntax:
            fetch("URL", {options})
            .then(res => res.json)
            .then(data => {})
        */

        fetch('http://localhost:4000/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data);
            if(typeof data.accessToken !== "undefined") {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            
                Swal.fire({
                    title: "Login is successful",
                    icon: 'success',
                    text: 'Welcome'
                })
            } else {
                Swal.fire({
                    title: "Authentication Failed",
                    icon: 'error',
                    text: 'Check your login credentials'
                })
            }
        })

        setEmail("");
        setPassword("");

        /*
        Syntax:
            localStorage.setItem("propertyName", value)
        */
        // localStorage.setItem("email", email)

        //Set the global user state to have properties from local storage
        // setUser({
        //     email: localStorage.getItem('email')
        // })

        const retrieveUserDetails = (token) => {
            fetch('http://localhost:4000/users/details', {
                headers: {
                    Authorization: `Bearer ${ token }`
                }
            })
            .then(res => res.json())
            .then(data => {
                // console.log(data)

                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            })
        }

    }    

    //show or hide password
    const showPassword = (e) => {
    e.preventDefault();
    setPasswordShown(passwordShown ? false : true)
    }
    //

    useEffect (() => {
        if(email !== "" && password !== ""){
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password])
    return (
    <Fragment>
    <AppNavbar/>
        {(user.isAdmin === true) ?
        <Navigate to='/admin'/>
        :
        (user.id !== null) ?
        <Navigate to="/"/>
        :
        <Container>
            <Title>Login Page</Title>
            <Container className="col col-10">
                <Form onSubmit={(e) => userLogin(e)}>
                    <Form.Label className="pt-2">Email:</Form.Label>
                    <Form.Group>
                    <InputGroup>
                            <Form.Control
                                type="email"
                                placeholder="Enter Email"
                                value={email}
                                onChange={e => setEmail(e.target.value)}
                                required
                            />
                            <InputGroup.Append>
                                <InputGroup.Text className="loginEnvelope">
                                <i><BsEnvelope/></i>
                                </InputGroup.Text>
                            </InputGroup.Append>
                    </InputGroup>
                    </Form.Group>

                    <Form.Label className="pt-2">Password:</Form.Label>
                    <Form.Group>
                    <InputGroup>
                            <Form.Control
                                type={passwordShown ? "text" : "password"}
                                placeholder="Enter Password"
                                value={password}
                                onChange={e => setPassword(e.target.value)}
                                required
                            />
                        <InputGroup.Append>
                            <InputGroup.Text>
                                {
                                    passwordShown ?
                                    <i onClick={e => showPassword(e)}><BsEye/></i>
                                    :
                                    <i onClick={e => showPassword(e)}><BsEyeSlash/></i>
                                }
                            </InputGroup.Text>
                        </InputGroup.Append>
                    </InputGroup>
                    </Form.Group>
                    {
                        isActive ?
                        <Button className="btn-block my-2" variant="info" type="submit">Submit</Button>
                        :
                        <Button className="btn-block my-2" variant="info" type="submit" disabled>Submit</Button>
                    }
                    
                </Form>
            </Container>
        </Container>}
    </Fragment>
    ) 
}
