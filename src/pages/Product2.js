
import { Container, Nav, Navbar } from "react-bootstrap";
import { Fragment, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import AdminProductCard from "../components/AdminProductCard";

export default function Product2() {

    const [products, setProducts] = useState([]);

    useEffect(() => {
		fetch("http://localhost:4000/products/all")
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setProducts(data.map(product => {
				return(
					<AdminProductCard key={product._id} productProps={product} />
					)
			}))
		})	
	}, [])

  return (
    <Fragment>
        <Navbar bg="info">
            <Navbar.Brand className="mx-5" as={Link} to='/admin'>Admin</Navbar.Brand>
            <Nav.Link as={Link} to='/logout' className="col d-flex justify-content-end px-5 text-danger">Logout</Nav.Link>
        </Navbar>
   <Container>
       {products}
   </Container>
    </Fragment>
  )
}
