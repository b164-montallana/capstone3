import { Fragment, useContext, useEffect, useState } from "react";
import UserContext from "../UserContext";
import { Container, Row, Col, Card, Button, Nav } from "react-bootstrap";
import { Link, useParams } from "react-router-dom";
import AppNavbar from "../components/AppNavbar";

export default function ProductView() {

    const { user  } = useContext(UserContext);

    // const navigate = useNavigate();

    const { productId } = useParams();

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [stock, setStock] = useState(0);



    useEffect(() => {
        console.log(productId)
        fetch(`http://localhost:4000/products/${productId}`)
        .then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setStock(data.stock)
		})


	}, [productId])

    

  return (
    <Fragment>
        <AppNavbar/>
        <Nav.Link className="m-5" as={Link} to='/products'>Back</Nav.Link>
        <Container className="mt-5">
            <Row>
                <Col lg={{span:6, offset:3}}>
                    <Card>
                        <Card.Body className="text-center">
                            <Card.Title>{name}</Card.Title>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>PHP {price}</Card.Subtitle>
                            <Card.Text>Stocks Available</Card.Text>
                            <Card.Text>{stock}</Card.Text>

                            {
                            user.id !== null ?
                            <Button variant="info">Add to cart</Button>
                            :
                            <Link className="btn btn-danger" to="/login">Log in</Link>

                            }

                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    </Fragment>
  )
}
