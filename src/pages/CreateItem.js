import { useContext, useState } from "react";
import UserContext from "../UserContext";
import { Form, Container, FormControl, Button} from "react-bootstrap";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";


export default function CreateItem() {

  const { user, setUser } = useContext(UserContext);

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [categories, setCategories] = useState("");
  const [stock, setStock] = useState(0);

  function addProduct(e) {
    e.preventDefault();

    fetch('http://localhost:4000/products/add', {
      method: "POST",
      headers: {
        "Content-Type": 'application/json',
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
        categories: categories,
        stock: stock
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      if(data === true) {
        setName("");
        setDescription("");
        setPrice("");
        setCategories("");
        setStock("");

        Swal.fire({
          icon: 'success'
        })
      } else {
        console.log()
        Swal.fire({
          icon: 'error'
        })
      }
    })
  }

  return (
    (user.isAdmin === false ) ?
    <Navigate to='/error'/>
    :
    <Container>
      <Form onSubmit={(e) => addProduct(e)}>
        <Form.Label>Product Name:</Form.Label>
        <Form.Group className="py-1">
          <FormControl
            type="text"
            placeholder="Product Name"
            value={name}
            onChange={e => setName(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Label>Description:</Form.Label>
        <Form.Group className="py-1">
          <FormControl
            type="text"
            placeholder="Product Description"
            value={description}
            onChange={e => setDescription(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Label>Price:</Form.Label>
        <Form.Group className="py-1">
          <FormControl
            type="number"
            placeholder="Product Price"
            value={price}
            onChange={e => setPrice(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Label>Categories:</Form.Label>
        <Form.Group className="py-1">
          <FormControl
            type="text"
            placeholder="Product Category"
            value={categories}
            onChange={e => setCategories(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Label>Stocks:</Form.Label>
        <Form.Group className="py-1">
          <FormControl
            type="number"
            placeholder="Stock"
            value={stock}
            onChange={e => setStock(e.target.value)}
            required
          />
        </Form.Group>

        <Button className="btn-block my-2" variant="info" type="submit">Submit</Button>
      </Form>
       
    </Container>
  )
}
